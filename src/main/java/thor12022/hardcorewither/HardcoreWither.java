package thor12022.hardcorewither;

import thor12022.hardcorewither.api.HardcoreWitherApiInternal;
import thor12022.hardcorewither.api.exceptions.HardcoreWitherException;
import thor12022.hardcorewither.client.gui.CreativeTabBaseMod;
import thor12022.hardcorewither.command.CommandManager;
import thor12022.hardcorewither.config.ConfigManager;
import thor12022.hardcorewither.core.HardcoreWitherApi;
import thor12022.hardcorewither.proxies.CommonProxy;
import net.minecraft.command.CommandHandler;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = ModInformation.ID, name = ModInformation.NAME, version = ModInformation.VERSION, dependencies = ModInformation.DEPEND)
public class HardcoreWither
{
   @Mod.Instance
   public static HardcoreWither INSTANCE;
   
   @SidedProxy(clientSide = ModInformation.CLIENTPROXY, serverSide = ModInformation.COMMONPROXY)
   public static CommonProxy proxy;

   public static final CreativeTabs       CREATIVE_TAB   =  new CreativeTabBaseMod(ModInformation.ID + ".creativeTab");
   public static final Random             RAND           =  new Random();
   public static final Logger             LOGGER         =  LogManager.getLogger(ModInformation.NAME);
   public static final ConfigManager      CONFIG         =  new ConfigManager(ModInformation.ID);
   public static final CommandManager     COMMAND        =  new CommandManager();
   public static final HardcoreWitherApi  API            =  new HardcoreWitherApi();
   
   public HardcoreWither()
   {
      try
      {
         HardcoreWitherApiInternal.setApi(API);
      }
      catch(HardcoreWitherException e)
      {
         LOGGER.error(e);
         LOGGER.fatal("Alight, here's the deal, I'm just minding my own business trying to Construct, but apprently I can't?!");
      }
   }
   
   @Mod.EventHandler
   public void preInit(@SuppressWarnings("unused") FMLPreInitializationEvent event)
   {
      proxy.preInit();
   }

   @Mod.EventHandler
   public void init(@SuppressWarnings("unused") FMLInitializationEvent event)
   {
      proxy.init();
   }

   @Mod.EventHandler
   public void serverStarting(FMLServerStartingEvent event)
   {
      COMMAND.register((CommandHandler)event.getServer().getCommandManager());
   }
}
