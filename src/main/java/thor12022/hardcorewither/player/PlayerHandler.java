package thor12022.hardcorewither.player;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class PlayerHandler
{

   public PlayerHandler()
   {
      
      MinecraftForge.EVENT_BUS.register(this);
      
      PlayerData.register();
      
   }
   
   @SubscribeEvent
   public void onMobConstructing(AttachCapabilitiesEvent.Entity event)
   {
      if(event.getEntity() instanceof EntityPlayer)
      {
         event.addCapability(PlayerData.NBT_TAG, PlayerData.getPlayerDataProvider());
      }
   }
   
   @SubscribeEvent
   public void onEntityDieing(PlayerEvent.Clone event)
   {
      if(event.isWasDeath())
      {
         PlayerData target = PlayerData.getPlayerData(event.getEntityPlayer());
         PlayerData source = PlayerData.getPlayerData(event.getOriginal());
         target.clone(source);
      }
   }
}
