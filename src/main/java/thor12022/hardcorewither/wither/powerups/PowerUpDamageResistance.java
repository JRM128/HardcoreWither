package thor12022.hardcorewither.wither.powerups;

import java.lang.invoke.MethodHandle;
import java.lang.reflect.Field;
import java.util.Map;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.init.MobEffects;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.api.IPowerUpEffect;
import thor12022.hardcorewither.config.Configurable;
import thor12022.hardcorewither.util.ReflectionUtils;

@Configurable // This class has no @Config members, but it parent class does
public class PowerUpDamageResistance extends BasePowerUp
{
   private final static int DEFAULT_MAX_STRENGTH = 3;   
   private final static int DEFAULT_MIN_LEVEL = 1;
   
   private static MethodHandle onNewPotionEffectMethod;
   private static Field  activePotionMapField;
   
   static 
   {
      try
      {
         onNewPotionEffectMethod = ReflectionUtils.findMethod(EntityLivingBase.class, new String[] {"func_70670_a", "onNewPotionEffect"}, PotionEffect.class);
      }
      catch(Exception exp)
      {
         HardcoreWither.LOGGER.error(exp);
         onNewPotionEffectMethod = null;
      }
      try
      {
         activePotionMapField = ReflectionHelper.findField(EntityLivingBase.class, new String[] {"field_70713_bf", "activePotionsMap"});
         activePotionMapField.setAccessible(true);
      }
      catch(Exception excp)
      {
         HardcoreWither.LOGGER.error(excp);
         activePotionMapField = null;
      }
   }
   
   static protected void setResistancePotionEffect(EntityWither wither, int strength)
   {
      final PotionEffect resistanceEffect = new PotionEffect(MobEffects.RESISTANCE, Integer.MAX_VALUE, strength);
      try
      {
         @SuppressWarnings("unchecked")
         final Map<Potion, PotionEffect> activePotionsMap = (Map<Potion, PotionEffect>) activePotionMapField.get(wither); 
         if (activePotionsMap.containsKey(resistanceEffect.getPotion()))
         {
            activePotionsMap.remove(resistanceEffect.getPotion());
         }
         activePotionsMap.put(resistanceEffect.getPotion(), resistanceEffect);
          
         onNewPotionEffectMethod.invoke(wither, resistanceEffect);
      }
      catch(Throwable e)
      {
         HardcoreWither.LOGGER.error(e);
      }
   }
   
   private class Data extends BasePowerUpEffect
   {

      public Data(EntityWither wither)
      {
         super(wither, PowerUpDamageResistance.this);
      }

      @Override
      public void setStrength(int strength)
      {
         super.setStrength(strength);
         setResistancePotionEffect(wither, strength);
      } 
   }
   
   public PowerUpDamageResistance()
   {
      super(DEFAULT_MIN_LEVEL, DEFAULT_MAX_STRENGTH);
      HardcoreWither.CONFIG.register(this);   
   }

   @Override
   public void updateWither(IPowerUpEffect data)
   {}

   @Override
   public void witherDied(IPowerUpEffect data)
   {}

   @Override
   public IPowerUpEffect getPowerUpEffect(EntityWither wither, int strength)
   {
      final BasePowerUpEffect data = new Data(wither);
      data.setStrength(strength);
      return data;
   }
   
   @Override
   public IPowerUpEffect restorePowerUp(EntityWither wither, NBTTagCompound stateNbt)
   { 
      final BasePowerUpEffect data = new Data(wither);
      data.deserializeNBT(stateNbt);
      return data;
   }
}
