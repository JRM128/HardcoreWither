package thor12022.hardcorewither.wither.powerups;

import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.api.IPowerUpEffect;
import thor12022.hardcorewither.config.Config;
import thor12022.hardcorewither.config.Configurable;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.nbt.NBTTagCompound;

@Configurable // This has no @Config member, but it's parent class does
public class PowerUpSpeedBoost extends BasePowerUp
{
   private final static int DEFAULT_MAX_STRENGTH = 6;
   private final static int DEFAULT_MIN_LEVEL = 3;
   
   @Config(comment="I'd really reccomend 1")
   private static int speedBoostMultiplier = 1;

   protected PowerUpSpeedBoost()
   {
      super(DEFAULT_MIN_LEVEL, DEFAULT_MAX_STRENGTH);
      HardcoreWither.CONFIG.register(this);
   }

   @Override
   public void updateWither(IPowerUpEffect data)
   {}

   @Override
   public void witherDied(IPowerUpEffect data)
   {}

   @Override
   public IPowerUpEffect getPowerUpEffect(EntityWither wither, int strength)
   {
      return null;
   }

   @Override
   public IPowerUpEffect restorePowerUp(EntityWither wither, NBTTagCompound stateNbt)
   {
      return null;
   }
}
