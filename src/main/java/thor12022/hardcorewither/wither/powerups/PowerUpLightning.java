package thor12022.hardcorewither.wither.powerups;

import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.api.IPowerUpEffect;
import thor12022.hardcorewither.config.Config;
import thor12022.hardcorewither.config.Configurable;
import net.minecraft.entity.Entity;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.nbt.NBTTagCompound;

@Configurable
public class PowerUpLightning extends BasePowerUp
{
   private final static int DEFAULT_MAX_STRENGTH = 20;
   private final static int DEFAULT_MIN_LEVEL = 1;
   
   @Config(minFloat = 1f, maxFloat = 5f)
   private static float lightningFrequencyMultiplier  = 1.1f;
   
   @Config(minFloat = 1f, maxFloat = 5f, comment = "0 is not random, 1 is more random")
   private static float lightningRandomness = 0.5f;
   
   @Config(minFloat = 1f, maxFloat = 5f, comment = "Avg number of ticks between lightning")
   private static int   lightningFequencyBase         = 100;
   
   @Config(minFloat = 0f, maxFloat = 5f, comment = "0 is prefect")
   private static float lightningInaccuracy = 0.5f;
   
   protected class Data extends BasePowerUpEffect
   {
      long nextTick;

      Data(EntityWither wither)
      {
         super(wither, PowerUpLightning.this);
      }
      
      public long getNextTick()
      {
         return nextTick;
      }

      void setNextRandomTick()
      {
         final long currentTick = wither.getEntityWorld().getTotalWorldTime();
         final long strengthBasedTick = (long)(lightningFequencyBase / (strength * lightningFrequencyMultiplier));
         final long modifier = (long)((HardcoreWither.RAND.nextGaussian() * lightningRandomness) * strengthBasedTick);
         nextTick =  currentTick + strengthBasedTick + modifier;
      }
      
      @Override
      public NBTTagCompound serializeNBT()
      {
         NBTTagCompound nbt = super.serializeNBT();
         nbt.setLong("nextTick", nextTick);
         return nbt;
      }

      @Override
      public void deserializeNBT(NBTTagCompound nbt)
      {
         super.deserializeNBT(nbt);
         nextTick = nbt.getLong("nextTick");
      }
   }
   
   public PowerUpLightning()
   {
      super(DEFAULT_MIN_LEVEL, DEFAULT_MAX_STRENGTH);
      HardcoreWither.CONFIG.register(this);
   }

   @Override
   public void updateWither(IPowerUpEffect effect)
   {
      Data lightningData = (Data)effect;
      if( lightningData.getWither().worldObj.getTotalWorldTime() > lightningData.nextTick )
      {
         int targetId = lightningData.getWither().getWatchedTargetId(0);
         if( targetId != -1)
         {
            Entity target = lightningData.getWither().worldObj.getEntityByID(targetId);
            if(target != null)
            {
               double lightningXPos = target.lastTickPosX + (8 * HardcoreWither.RAND.nextGaussian() * lightningInaccuracy);
               double lightningYPos = target.lastTickPosY + (8 * HardcoreWither.RAND.nextGaussian() * lightningInaccuracy);
               double lightningZPos = target.lastTickPosZ + (8 * HardcoreWither.RAND.nextGaussian() * lightningInaccuracy);
               //! @todo I hear rumours that this causes the sound effect Server-Wide
               lightningData.getWither().worldObj.addWeatherEffect(new EntityLightningBolt(lightningData.getWither().worldObj, lightningXPos, lightningYPos, lightningZPos, false));
               lightningData.setNextRandomTick();
            }
         }
      }
   }

   @Override
   public void witherDied(IPowerUpEffect data)
   {}

   @Override
   public IPowerUpEffect getPowerUpEffect(EntityWither wither, int strength)
   {
      final Data data = new Data(wither);
      data.setStrength(strength);
      data.setNextRandomTick(); 
      return data;
   }
   
   @Override
   public IPowerUpEffect restorePowerUp(EntityWither wither, NBTTagCompound stateNbt)
   {   
      final Data data = new Data(wither);
      data.deserializeNBT(stateNbt);
      return data;
   }
}
