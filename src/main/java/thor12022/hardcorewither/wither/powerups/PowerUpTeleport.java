package thor12022.hardcorewither.wither.powerups;

import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.api.IPowerUpEffect;
import thor12022.hardcorewither.config.Config;
import thor12022.hardcorewither.config.Configurable;
import net.minecraft.entity.Entity;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.Vec3d;

@Configurable
public class PowerUpTeleport extends BasePowerUp
{
   private final static int DEFAULT_MAX_STRENGTH = 20;
   private final static int DEFAULT_MIN_LEVEL = 4;
   
   @Config(minFloat = 1f, maxFloat = 10f)
   private static float teleportFrequencyMultiplier = 1.1f;
   
   @Config(minFloat = 0f, maxFloat = 10f, comment = "0 is not random, 1 is more random")
   private static float teleportRandomness = 0.5f;
   
   @Config(minInt = 20, comment = "Avg number of ticks between teleport attempt")
   private static int   teleportFequencyBase = 100;

   @Config(minInt = 0, maxInt = 10, comment = "Number of ticks in a row to retry a teleport attempt")
   private static int   teleportRetryTicks = 5;
   
   @Config(minFloat = 0f, maxFloat = 10f, comment = "0 is prefect")
   private static float teleportInaccuracy = 4f;
   
   @Config(comment="Less likely that the Wither may get stuck, but may affect performance")
   private static boolean requireLineOfSightToTarget = false;
   
   protected class Data extends BasePowerUpEffect
   {
      long nextTick;
      int retryNumber = 0;

      Data(EntityWither wither)
      {
         super(wither, PowerUpTeleport.this);
      }
      
      void setNextRandomTick()
      {
         setNextRandomTick(true);
      }
      
      void setNextRandomTick(boolean successfulAttempt)
      {
         if(successfulAttempt || retryNumber >= teleportRetryTicks)
         {
            retryNumber = 0;
            final long strengthBasedTick = (long) (teleportFequencyBase / (strength * teleportFrequencyMultiplier));
            final long modifier = (long) ((HardcoreWither.RAND.nextGaussian() * teleportRandomness) * strengthBasedTick);
            nextTick = wither.getEntityWorld().getTotalWorldTime() + strengthBasedTick + modifier;
         }
         else if(retryNumber < teleportRetryTicks)
         {
            retryNumber++;
            nextTick = wither.getEntityWorld().getTotalWorldTime() + 1;
         }
      }
      
      @Override
      public NBTTagCompound serializeNBT()
      {
         NBTTagCompound nbt = new NBTTagCompound();
         nbt.setLong("nextTick", nextTick);
         nbt.setInteger("retryNumber", retryNumber);
         return nbt;
      }

      @Override
      public void deserializeNBT(NBTTagCompound nbt)
      {
         nextTick = nbt.getLong("nextTick");
         retryNumber = nbt.getInteger("retryNumber");
      }
   }
   
   
   public PowerUpTeleport()
   {
      super(DEFAULT_MIN_LEVEL, DEFAULT_MAX_STRENGTH);
      HardcoreWither.CONFIG.register(this);   
   }

   @Override
   public void updateWither(IPowerUpEffect data)
   {
      final Data stateData = (Data)data;
      if( stateData.wither.getInvulTime() <= 0 &&  stateData.wither.worldObj.getTotalWorldTime() > stateData.nextTick )
      {
         final int targetId = stateData.wither.getWatchedTargetId(0);
         if( targetId != -1)
         {
            Entity target = stateData.wither.worldObj.getEntityByID(targetId);
            if(target != null)
            {
               final int meanDistance = stateData.wither.getDistanceSqToEntity(target) >= 256D ? 16 : 8;
               final double teleportXPos = target.posX + (meanDistance * (HardcoreWither.RAND.nextBoolean() ? 1 : -1)) + (teleportInaccuracy * ((HardcoreWither.RAND.nextFloat() * 2f) - 1));
               double teleportYPos = target.posY + (meanDistance * (HardcoreWither.RAND.nextBoolean() ? 1 : -1)) + (teleportInaccuracy * ((HardcoreWither.RAND.nextFloat() * 2f) - 1));
               final double teleportZPos = target.posZ + (meanDistance * (HardcoreWither.RAND.nextBoolean() ? 1 : -1)) + (teleportInaccuracy * ((HardcoreWither.RAND.nextFloat() * 2f) - 1));
               if(teleportYPos < 0)
               {
                  teleportYPos = 0;
               }
               
               boolean successfulTeleport = true;
               if(requireLineOfSightToTarget && stateData.getWither().getAttackTarget() != null)
               {
                  final Vec3d vecWither = new Vec3d(stateData.getWither().posX, stateData.getWither().posY + stateData.getWither().getEyeHeight(), stateData.getWither().posZ);
                  final Vec3d vecTarget = new Vec3d(stateData.getWither().getAttackTarget().posX, stateData.getWither().getAttackTarget().posY + stateData.getWither().getAttackTarget().getEyeHeight(), stateData.getWither().getAttackTarget().posZ);
                  successfulTeleport = stateData.getWither().getEntityWorld().rayTraceBlocks(vecWither, vecTarget, false, true, false) == null;
               }
               if(successfulTeleport)
               {
                  successfulTeleport = stateData.wither.attemptTeleport(teleportXPos,teleportYPos, teleportZPos);
                  if(successfulTeleport)
                  {
                     stateData.getWither().getEntityWorld().playSound(null, stateData.getWither().prevPosX, stateData.getWither().prevPosY, stateData.getWither().prevPosZ, SoundEvents.ENTITY_ENDERMEN_TELEPORT, stateData.getWither().getSoundCategory(), 1.0F, 1.0F);
                     stateData.getWither().playSound(SoundEvents.ENTITY_ENDERMEN_TELEPORT, 1.0F, 1.0F);
                     //Path path = stateData.wither.getNavigator().getPathToPos(stateData.wither.getPosition());
                     //stateData.wither.getNavigator().setPath(path, 0d);
                  }
               }
               stateData.setNextRandomTick(successfulTeleport);
            }
         }
      }
   }

   @Override
   public void witherDied(IPowerUpEffect data)
   {}

   @Override
   public IPowerUpEffect getPowerUpEffect(EntityWither wither, int strength)
   {
      final Data data = new Data(wither);
      data.setStrength(strength);
      data.setNextRandomTick(); 
      return data;
   }
   
   @Override
   public IPowerUpEffect restorePowerUp(EntityWither wither, NBTTagCompound stateNbt)
   { 
      final Data data = new Data(wither);
      data.deserializeNBT(stateNbt);
      return data;
   }
}
