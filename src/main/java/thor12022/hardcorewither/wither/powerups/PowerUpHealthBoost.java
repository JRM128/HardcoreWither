package thor12022.hardcorewither.wither.powerups;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.nbt.NBTTagCompound;
import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.api.IPowerUpEffect;
import thor12022.hardcorewither.config.Config;
import thor12022.hardcorewither.config.Configurable;

@Configurable
public class PowerUpHealthBoost extends BasePowerUp
{
   private final static int DEFAULT_MAX_STRENGTH = 128;
   private final static int DEFAULT_MIN_LEVEL = 1;

   @Config(minFloat = 1f, maxFloat = 10f)
   private static float healthBoostMultiplier = 1.1f;

   static protected void updateWitherHealth(HealthBoostPowerUpEffect effect)
   {
      double newHealth = effect.initialHealth * healthBoostMultiplier * effect.strength;
      effect.wither.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(newHealth);
   }
   
   private class HealthBoostPowerUpEffect extends BasePowerUpEffect
   {
      // In case some other mod changes it. See Mojang? Don't hard-code everything with magic numbers.
      //    Though, to be fair, it may not be a magic number, that's probably the decompiler's fault
      final double initialHealth;
      
      public HealthBoostPowerUpEffect(EntityWither wither)
      {
         super(wither, PowerUpHealthBoost.this);
         initialHealth = wither.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue();
      }

      @Override
      public void setStrength(int strength)
      {
         super.setStrength(strength);
         updateWitherHealth(this);
      } 
   }
   
   public PowerUpHealthBoost()
   {
      super(DEFAULT_MIN_LEVEL, DEFAULT_MAX_STRENGTH);
      HardcoreWither.CONFIG.register(this);
   }

   @Override
   public void updateWither(IPowerUpEffect data)
   {
      if(data.getWither().isServerWorld())
      {
         if(data.getWither().getInvulTime() >= 20)
         {
            float baseHealth = (float) data.getWither().getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).getBaseValue();
            // ! @todo this is close, but not quite right, it is a bit too fast
            // at higher levels
            data.getWither().heal((((baseHealth * (2f / 3f)) - 200) / 200));
         }
      }
   }

   @Override
   public void witherDied(IPowerUpEffect data)
   {}

   @Override
   public IPowerUpEffect getPowerUpEffect(EntityWither wither, int strength)
   {      
      final BasePowerUpEffect data = new HealthBoostPowerUpEffect(wither);
      data.setStrength(strength);
      return data;
   }
   
   @Override
   public IPowerUpEffect restorePowerUp(EntityWither wither, NBTTagCompound stateNbt)
   {   
      final BasePowerUpEffect data = new HealthBoostPowerUpEffect(wither);
      data.deserializeNBT(stateNbt);
      return data;
   }
}
