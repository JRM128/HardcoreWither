package thor12022.hardcorewither.items;

import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.ModInformation;
import thor12022.hardcorewither.config.Config;
import thor12022.hardcorewither.config.Configurable;
import thor12022.hardcorewither.util.I18n;

import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Configurable
public class ItemDeathStick extends Item implements IItem
{
   @Config(comment="Death Stick will kill anything it hits. Creative ONLY")
   private static boolean enableDeathStick = true;
   
   private final static String NAME = "deathStick";
      
   public ItemDeathStick()
   {
      setUnlocalizedName(ModInformation.ID + "." + NAME);
      setCreativeTab(HardcoreWither.CREATIVE_TAB);
      HardcoreWither.CONFIG.register(this);
      MinecraftForge.EVENT_BUS.register(this);
   }
   
   @Override
   @SideOnly(Side.CLIENT)
   public boolean hasEffect(ItemStack stack)
   {
      try
      {
         return Minecraft.getMinecraft().thePlayer.isCreative();
      }
      catch(Exception e)
      {
         return true;
      }
   }

   @Override
   @SideOnly(Side.CLIENT)
   public void addInformation (ItemStack stack, EntityPlayer player, List<String> list, boolean par4)
   {
      list.add(TextFormatting.LIGHT_PURPLE.toString() + TextFormatting.ITALIC + I18n.localize("tooltip." + ModInformation.ID + ".creative" ));
   }

   @Override
   public String name()
   {
      return NAME;
   }

   @Override
   public void registerItem()
   {
      setRegistryName(NAME);
      GameRegistry.register(this);
   }

   @Override
   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(Items.STICK.getRegistryName(), ""));
   }

   @Override
   public void registerRecipe()
   {}

   @Override
   public boolean isEnabled()
   {
      return enableDeathStick;
   }

   public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity)
   {
      if(!player.getEntityWorld().isRemote && player.isCreative() && stack.getItem() == this)
      {
         entity.setDead();
         return false;
      }
      return super.onLeftClickEntity(stack, player, entity);
   }
}
