package thor12022.hardcorewither.entity;

import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.ModInformation;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.SkeletonType;
import net.minecraft.init.Items;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import net.minecraftforge.fml.relauncher.ReflectionHelper.UnableToFindFieldException;

public class EntitySkeletonMinion extends EntitySkeleton
{
   public static final String UNLOCALIZED_NAME = "SkeletonMinion";
   public static final String LOCALIZED_NAME = ModInformation.ID + "." + UNLOCALIZED_NAME;
   
   private EntityAIAttackMelee aiAttackOnCollide;

   public EntitySkeletonMinion(World world)
   {
      super(world);
      setSkeletonType(SkeletonType.WITHER);
      try
      {
         aiAttackOnCollide = (EntityAIAttackMelee)ReflectionHelper.findField(super.getClass(), new  String[] {"aiAttackOnCollide"}).get(this);
      }
      catch(IllegalArgumentException | IllegalAccessException | UnableToFindFieldException e)
      {
         HardcoreWither.LOGGER.error(e);
         HardcoreWither.LOGGER.error("Cannot properly use reflection upon EntitySkeleton#aiAttackOnCollide, something has changed. -_-");
         aiAttackOnCollide = null;
      }
      
   }
   
   @Override
   public EnumCreatureAttribute getCreatureAttribute()
   {
       return EnumCreatureAttribute.UNDEAD;
   }
   
   @Override
   public boolean attackEntityFrom(DamageSource p_70097_1_, float p_70097_2_)
   {
      if (p_70097_1_.getEntity() != null && p_70097_1_.getEntity().getClass() == EntityWither.class)
      {
         return false;
      }
      return super.attackEntityFrom(p_70097_1_, p_70097_2_);
   }
   
   @Override
   protected void dropFewItems(boolean p_70628_1_, int p_70628_2_)
   {}
   
   @Override
   public IEntityLivingData onInitialSpawn(DifficultyInstance difficulty, IEntityLivingData livingData)
   {
      // Apply the Wither Skeleton's standard attributes
      if(aiAttackOnCollide != null)
      {
         this.tasks.addTask(4, aiAttackOnCollide);
      }
      this.setItemStackToSlot(EntityEquipmentSlot.MAINHAND, new ItemStack(Items.STONE_SWORD));
      this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(4.0D);
      this.setCanPickUpLoot(false);
      return livingData;
   }
   
   @Override
   protected boolean isValidLightLevel()
   {
       return true;
   }
}
