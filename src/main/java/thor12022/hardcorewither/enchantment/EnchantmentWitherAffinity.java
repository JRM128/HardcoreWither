package thor12022.hardcorewither.enchantment;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnumEnchantmentType;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import thor12022.hardcorewither.ModInformation;

public class EnchantmentWitherAffinity extends Enchantment
{
   static final String NAME = "witherAffinity";
    
   EnchantmentWitherAffinity()
   {
      super(Enchantment.Rarity.VERY_RARE, EnumEnchantmentType.WEAPON, new EntityEquipmentSlot[] {EntityEquipmentSlot.MAINHAND});
      this.setName(ModInformation.ID + "." + NAME);
      this.setRegistryName(NAME);
   }
   
   @Override
   public boolean canApply(ItemStack stack)
   {
      return false;
   }

   @Override
   public boolean canApplyTogether(Enchantment enchant)
   {
      return false;
   }
   
   @Override
   public boolean isAllowedOnBooks()
   {
       return false;
   }
}