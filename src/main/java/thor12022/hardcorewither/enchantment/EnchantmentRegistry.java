package thor12022.hardcorewither.enchantment;

import net.minecraft.enchantment.Enchantment;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class EnchantmentRegistry
{
   public static Enchantment witherAffinity = new EnchantmentWitherAffinity();
   
   public static void register()
   {
      GameRegistry.register(witherAffinity);
   }
}
