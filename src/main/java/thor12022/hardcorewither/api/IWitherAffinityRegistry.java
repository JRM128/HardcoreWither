package thor12022.hardcorewither.api;

import java.util.Collection;

import net.minecraft.item.Item;

public interface IWitherAffinityRegistry
{
   /**
    * @return false if registration failed, or duplicate Item
    * @pre The server has not been started yet (Constructing, Preinit, or init)
    * @todo what about sub-items?
    */
   boolean register(Item item);
   
   /**
    * @return a collection of all Items that have been registered
    */
   Collection<Item> getAll();
   
   /**
    * @return true if the given Item has been registered
    */
   boolean isRegistered(Item item);
      
}
