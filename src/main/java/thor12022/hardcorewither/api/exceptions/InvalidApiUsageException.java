package thor12022.hardcorewither.api.exceptions;

/**
 * Thrown if the Hardcore Wither API is used improperly, e.g. called before being initialized
 *
 */
public class InvalidApiUsageException extends HardcoreWitherException
{
  private static final long serialVersionUID = 1L;

   public InvalidApiUsageException(String string)
   {
      super("Invalid API Usage: " + string);
   }

}
