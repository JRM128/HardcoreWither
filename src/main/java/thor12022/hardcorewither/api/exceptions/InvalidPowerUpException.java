package thor12022.hardcorewither.api.exceptions;

/**
   @deprecated Using a Forge Registry now, have no way to throw this without wrapping it. Remove all uses.
**/
@Deprecated
public class InvalidPowerUpException extends HardcoreWitherException
{
   private static final long serialVersionUID = 1L;
      
   public InvalidPowerUpException(String name)
   {
      super("Id(" + name + ")not for a valid PowerUp");
   }
}
