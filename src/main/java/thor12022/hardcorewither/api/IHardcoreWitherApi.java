package thor12022.hardcorewither.api;

import net.minecraftforge.fml.common.registry.FMLControlledNamespacedRegistry;

/**
 * This should not be implemented
 */
public interface IHardcoreWitherApi
{
   
   FMLControlledNamespacedRegistry<IPowerUp> getPowerUpRegistry();  
   
   IWitherAffinityRegistry getWitherAffinityRegistry();
}
