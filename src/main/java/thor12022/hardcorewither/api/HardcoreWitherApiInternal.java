package thor12022.hardcorewither.api;

import net.minecraftforge.fml.common.Loader;
import thor12022.hardcorewither.api.exceptions.HardcoreWitherException;

/**
 * The should be removed from the API Jar, and should not be used
 *
 */
public class HardcoreWitherApiInternal
{
   public static void setApi(IHardcoreWitherApi api) throws HardcoreWitherException
   {
      if(Loader.instance().activeModContainer().getMod() == HardcoreWither.instance)
      {
         HardcoreWither.instance = api;
      }
      else
      {
         throw new HardcoreWitherException("Improper API Initialization");
      }
   }
}
