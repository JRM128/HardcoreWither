package thor12022.hardcorewither.potions;

import java.util.Map;
import java.util.Set;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.ModInformation;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.attributes.AbstractAttributeMap;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PotionAntiWither extends Potion
{
   public static final String NAME = "antiWither";
   
   //! @todo change to store data attached to entity, not statically here
   private static final Map<EntityLivingBase,Integer> AFFECTED_ENTITIES = Maps.newHashMap();
   private static final Set<EntityLivingBase> AFFECTED_WITHERED_ENTITIES = Sets.newHashSet();
   
   public PotionAntiWither()
   {
      super(false, 1310740);
      this.setPotionName("potion." + ModInformation.ID + "." + NAME);
      setRegistryName(NAME);
      this.setIconIndex(1, 0);
   }
   
   public static boolean HasEntityBeenWithered(EntityLivingBase entity)
   {
      return AFFECTED_WITHERED_ENTITIES.contains(entity);
   }
   
   @Override
   public void applyAttributesModifiersToEntity(EntityLivingBase entityLiving, AbstractAttributeMap map, int par3) 
   {
      if( !AFFECTED_ENTITIES.containsKey(entityLiving))
      {
         AFFECTED_ENTITIES.put(entityLiving, -1);
      }
   }

   @Override
   public void removeAttributesModifiersFromEntity(EntityLivingBase entityLiving, AbstractAttributeMap map, int par3)
   {
      if(AFFECTED_ENTITIES.containsKey(entityLiving))
      {
         AFFECTED_ENTITIES.remove(entityLiving);
      }
      if(AFFECTED_WITHERED_ENTITIES.contains(entityLiving))
      {
         AFFECTED_WITHERED_ENTITIES.remove(entityLiving);
      }
   }
   
   @Override
   @SideOnly(Side.CLIENT)
   public int getStatusIconIndex()
   {
      Minecraft.getMinecraft().renderEngine.bindTexture(new ResourceLocation(ModInformation.ID + ":textures/guis/icons.png"));
      return 1;
   }
   
   @Override
   public boolean isReady(int tick, int amplifier)
   {
      return true;
   }

   @Override
   public void performEffect(EntityLivingBase entityLivingBaseIn, int p_76394_2_)
   {
      PotionEffect witherEffect = entityLivingBaseIn.getActivePotionEffect(MobEffects.WITHER);
      if(witherEffect != null)
      {
         if( witherEffect.getDuration() > AFFECTED_ENTITIES.get(entityLivingBaseIn) )
         {
            int newDuration = ( witherEffect.getDuration() - AFFECTED_ENTITIES.get(entityLivingBaseIn) ) / ( 2 * (p_76394_2_ + 1));
            HardcoreWither.LOGGER.debug("Anti-Wither reducing Wither effect to " + newDuration/20 + " seconds for " + entityLivingBaseIn.getDisplayName());
            int amplifier = witherEffect.getAmplifier();
            entityLivingBaseIn.removePotionEffect(MobEffects.WITHER);
            entityLivingBaseIn.addPotionEffect(new PotionEffect(MobEffects.WITHER, newDuration, amplifier));
            AFFECTED_ENTITIES.put(entityLivingBaseIn, newDuration);
            AFFECTED_WITHERED_ENTITIES.add(entityLivingBaseIn);
         }
         else
         {
            AFFECTED_ENTITIES.put(entityLivingBaseIn, witherEffect.getDuration());
         }
      }
   }

   @Override
   public void affectEntity(Entity source, Entity indirectSource, EntityLivingBase entityLivingBaseIn, int amplifier,
                            double health)
   {
      //! @todo Handle Lingering version?
   }   
}
